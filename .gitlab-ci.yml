variables:
  CONTAINER_COMMIT_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  CONTAINER_LATEST_IMAGE: $CI_REGISTRY_IMAGE:latest
  PYTHON3_IMAGE: python:alpine

stages:
  - syntax
  - sast
  - unittests
  - build
  - inttests
  - release

.onlyBranchesOrMergeRequests:
  rules:
    - if: '$CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      when: always

.onlyMainBranch:
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: always

.onlyTag:
  rules:
    - if: '$CI_COMMIT_TAG'
      when: always

PyCodeStyle:
  image: $PYTHON3_IMAGE
  stage: syntax
  script:
    - pip install -r requirements.txt
    - pycodestyle app.py
  extends: .onlyBranchesOrMergeRequests

Pylint:
  image: $PYTHON3_IMAGE
  stage: syntax
  script:
    - pip install -r requirements.txt
    - pylint app.py
  extends: .onlyBranchesOrMergeRequests

Static Code Analysis Tests:
  stage: sast
  image: $PYTHON3_IMAGE
  script:
    - pip install -r requirements.txt
    - bandit app.py
  extends: .onlyBranchesOrMergeRequests

Launch Unit Tests:
  stage: unittests
  image: $PYTHON3_IMAGE
  script:
    - pip install -r requirements.txt
    - python -m pytest --junit-xml=pytest-report.xml
  extends: .onlyBranchesOrMergeRequests

Build Image:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [ "" ]
  script:
    - test -z "$DOCKER_CONFIG" && export DOCKER_CONFIG=/kaniko/.docker
    - |
      cat > "${DOCKER_CONFIG}/config.json" <<EOF
      { "auths": { "${CI_REGISTRY}": { "username": "gitlab-ci-token", "password": "${CI_JOB_TOKEN}" } } }
      EOF
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --build-arg APP_VERSION="${CI_COMMIT_SHORT_SHA}"
      --destination "${CONTAINER_COMMIT_IMAGE}"
      --destination "${CONTAINER_LATEST_IMAGE}"
  extends: .onlyMainBranch

Integ tests:
  image:
    name: dduportal/bats:0.4.0
    entrypoint: [ "" ]
  stage: inttests
  services:
    - name: $CONTAINER_COMMIT_IMAGE
      alias: my-app
  script:
    - chmod 740 $CI_PROJECT_DIR/gitlab-ci-scripts/test_bats.sh
    - bats $CI_PROJECT_DIR/gitlab-ci-scripts/test_bats.sh
  extends: .onlyMainBranch

Tag Image Docker for Release:
  stage: release
  image:
    name: solsson/crane@sha256:58647d756b9008f312827227d0344ca2a7439c99222668c95e93d99dcc94d9ac
    entrypoint: [ "" ]
  script:
    - test -z "$DOCKER_CONFIG" && export DOCKER_CONFIG=/
    - |
      cat > "${DOCKER_CONFIG}/config.json" <<EOF
      { "auths": { "${CI_REGISTRY}": { "username": "gitlab-ci-token", "password": "${CI_JOB_TOKEN}" } } }
      EOF
    - crane cp "${CONTAINER_COMMIT_IMAGE}" "$CI_REGISTRY_IMAGE:${CI_COMMIT_TAG}"
  extends: .onlyTag